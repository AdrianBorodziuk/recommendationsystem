<?php

namespace OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserOrder
 *
 * @ORM\Table(name="user_order")
 * @ORM\Entity(repositoryClass="OrderBundle\Repository\UserOrderRepository")
 */
class UserOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\User", inversedBy="orders")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $storeUrl;
    
    /**
     * @ORM\Column(type="decimal", length=64, scale=2)
     */
    private $sum;
    
    /**
     * @ORM\Column(type="decimal", length=64, scale=2)
     */
    private $provision;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAdded;

    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $customer;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set storeUrl
     *
     * @param string $storeUrl
     *
     * @return UserOrder
     */
    public function setStoreUrl($storeUrl)
    {
        $this->storeUrl = $storeUrl;

        return $this;
    }

    /**
     * Get storeUrl
     *
     * @return string
     */
    public function getStoreUrl()
    {
        return $this->storeUrl;
    }

    /**
     * Set sum
     *
     * @param string $sum
     *
     * @return UserOrder
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * Get sum
     *
     * @return string
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * Set provision
     *
     * @param integer $provision
     *
     * @return UserOrder
     */
    public function setProvision($provision)
    {
        $this->provision = $provision;

        return $this;
    }

    /**
     * Get provision
     *
     * @return integer
     */
    public function getProvision()
    {
        return $this->provision;
    }

    /**
     * Set dateAdded
     *
     * @return UserOrder
     */
    public function setDateAdded()
    {
        $this->dateAdded = new \DateTime("now");

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return UserOrder
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }
    
    public function getCustomer()
    {
        return $this->customer;
    }
}
