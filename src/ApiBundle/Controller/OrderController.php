<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use OrderBundle\Entity\UserOrder;
use ApiBundle\Form\OrderType;

class OrderController extends FOSRestController
{
    
    /**
     * @POST("/api/order/add")
     */
    public function addAction(Request $request) {
        $order = new UserOrder();
        $view = View::create();
        
        $form = $this->createForm(OrderType::class, $order, array(
            'method' => 'POST'
        ));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('UserBundle:User')
                    ->findOneByStoreUrl($order->getStoreUrl());
            
            if(!$user instanceof User) {
                $view->setStatusCode(200);
                $view->setData("This store is not registered in system.");
            }
            
            $order->setProvision((double)$form->get('sum')->getData() * 0.06);
            $order->setDateAdded();
            $order->setUser($user);
                        
            $em->persist($order);
            $em->flush();
        } else {
            $view->setStatusCode(403);
            $view->setData("Incorrect parameters given.");
            return $this->handleView($view);
        }
        
        
        $view->setStatusCode(200);
        $view->setData("Order added successfuly.");
        return $this->handleView($view);
    }
    /*
     * 
<form action="http://lokalny.system.com/app_dev.php/api/order/add" method="POST">
<input type="text" name="order[customer]">
<input type="text" name="order[sum]">
<input type="text" name="order[storeUrl]">
<input type="submit"/>
</form>
     */
    
}