<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use BusinessSectorsBundle\Entity\BusinessSector;

class ProductsController extends FOSRestController
{
    
    /**
     * @GET("/api/products/get")
     * @QueryParam(name="sectors", strict=true, description="Business Sectors")
     */
    public function getAction(ParamFetcher $paramFetcher, Request $request) {
        $sectors = explode(',', base64_decode($paramFetcher->get('sectors')));
        $view = View::create();
        
        $em = $this->getDoctrine()->getManager();
        $businessSectors = array();
        
        foreach($sectors as $sector) {
            $result = $em->getRepository('BusinessSectorsBundle:BusinessSector')
                    ->findOneByName($sector);

            if($result instanceof \BusinessSectorsBundle\Entity\BusinessSector) {
                $businessSectors[] = $sector;
            }
        }
        
        if(count($businessSectors) < 1) {
            $view->setStatusCode(404);
            $view->setData("No business sectors specified or sectors don't exist.");
            return $this->handleView($view);
        }
        
        $query = $em->getRepository('ProductBundle:Product')
                ->createQueryBuilder('p')
                ->join('p.user', 'u')
                ->join('u.businessSectors', 'us')
                ->join('us.businessSector', 's');
        
        $where = '';
        foreach($businessSectors as $key => $businessSector) {
            $where .= "s.name = '" . ucwords($businessSector)."'";
            if(isset($businessSectors[$key + 1])) {
                $where .= ' OR ';
            }
        }
        
        $products = $query->where($where)
                ->getQuery()
                ->getResult();
        
        if($products == false || is_null($products)) {
            $view->setStatusCode(404);
            $view->setData("No products are available right now for this criteria.");
            return $this->handleView($view);
        }
                
        $view->setStatusCode(200);
        $view->setData($this->getRandomProducts($products, $request));

        return $this->handleView($view);
    }
    
    private function getRandomProducts($products, $request) {
        $randomIds = array();
        $max = count($products) >= 4 ? 4 : count($products);
        
        for($i = 0; $i <= $max; $i++) {
            $randomNum = null;
            do {
                $randomNum = rand(0, count($products) - 1);
            } while(in_array($randomNum, $randomIds));
            $randomIds[] = $randomNum;
        }
        
        $randomProducts = array();
        $counter = 0;
        
        foreach($products as $product) {
            if(in_array($counter, $randomIds)) {
                $randomProducts[] = $product;
            }
            $counter++;
        }
        
        shuffle($randomProducts);
        
        return $this->prepareData($randomProducts, $request);
    }
    
    private function prepareData($products, $request) {
        $data = array();
        
        foreach($products as $product) {
            $data[] = array(
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'productUrl' => $product->getUrl(),
                'imageUrl' => 'http://' . $request->getHttpHost() . '/' . $product->getImageUrl()
            );
        }
        
        return $data;
    }
}
