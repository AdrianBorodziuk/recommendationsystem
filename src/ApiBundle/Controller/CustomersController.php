<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;

class CustomersController extends FOSRestController
{
    
    public function addAction() {
        $view = $this->view(array(123), 200);
        return $this->handleView($view);
    }
    
    /**
     * @GET("/api/customers/discount")
     * @QueryParam(name="storeUrl", strict=true, description="Store URL")
     */
    public function discountAction(ParamFetcher $paramFetcher) {
        $storeUrl = urldecode($paramFetcher->get('storeUrl'));
        $view = View::create();
        $em = $this->getDoctrine()->getManager();
        
        if($storeUrl == '') {
            $view->setStatusCode(404);
            $view->setData('No store URL specified.');
            return $this->handleView($view);
        }
        $customer = $em->getRepository('UserBundle:User')->findOneByStoreUrl($storeUrl);
        
        if(!$customer || is_null($customer) || !$customer->getDiscountCode()) {
            $view->setStatusCode(404);
            $view->setData('Given store URL does not exist in system or user does not have discount code set.');
            return $this->handleView($view);
        }
        
        $view->setStatusCode(200);
        $view->setData(array('discount_code' => $customer->getDiscountCode()));
        return $this->handleView($view);
    }
}