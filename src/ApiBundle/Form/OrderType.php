<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Constraints\File;
use OrderBundle\Entity\UserOrder;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod($options['method'])
            ->add('storeUrl', TextType::class, array(
                'constraints' => array(
//                    new NotBlank(),  
//                    new Url()
            )))
            ->add('sum', IntegerType::class, array(
                'constraints' => array(
//                    new GreaterThan(0),
//                    new NotBlank()
            )))
            ->add('customer', TextType::class, array(
                'constraints' => array(
//                    new NotBlank(),  
            )));
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OrderBundle\Entity\UserOrder',
                  'csrf_protection' => false,
        ));
    }
}

