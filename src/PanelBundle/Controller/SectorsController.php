<?php

namespace PanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use BusinessSectorsBundle\Entity\BusinessSector;
use BusinessSectorsBundle\Entity\UserBusinessSector;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SectorsController extends Controller
{
    
    /**
     * @Route("/panel/sectors")
     */
    public function indexAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }
        
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        
        $choices = $em->getRepository('BusinessSectorsBundle\Entity\BusinessSector')
                ->findAll();
        
        $default = array();
        foreach($user->getBusinessSectors() as $userSector) {
            $default[] = $userSector->getBusinessSector();
        }

        $form = $this->createFormBuilder($user)
            ->setAction($this->generateUrl('panel_sectors_index'))
            ->setMethod('POST')
            ->add('tmpBusinessSectors', EntityType::class, [
                'class' => 'BusinessSectorsBundle:BusinessSector',
                'label' => 'Your business sectors',
                'data' => $default,
                'choices' => $choices,
                'choice_label' => function($businessSector, $key, $index) {
                    /** @var BusinessSector $businessSector */
                    return $businessSector->getName();
                },
                'expanded' => false,
                'multiple' => true,
            ])
            ->add('submit', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary btn-md saveButton'),
                'label' => 'Save',
            ))
            ->getForm();
                
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $entities = $em->getRepository('BusinessSectorsBundle\Entity\UserBusinessSector')
                ->createQueryBuilder('b')
                ->join('b.user', 'u')
                ->where('u.id = :uid')
                ->setParameter('uid', $user->getId())
                ->getQuery()
                ->getResult();
            
            foreach($entities as $entity) {
                    $em->remove($entity);
                }
            
            foreach($form->get('tmpBusinessSectors')->getData() as $businessSector) {  
                $userBusinessSector = new UserBusinessSector();
                $userBusinessSector->setUser($user);
                $userBusinessSector->setBusinessSector($businessSector);
                
                $user->addBusinessSector($userBusinessSector);
                $em->persist($userBusinessSector);
            }
            $em->persist($user);
            $em->flush();
            
            $this->addFlash(
                'success',
                'Your business sectors have been saved.'
            );
        }

        return $this->render('PanelBundle:Sectors:index.html.twig', 
            array("form" => $form->createView()));
    }
}

