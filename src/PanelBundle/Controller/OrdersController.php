<?php

namespace PanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use UserBundle\Entity\User;
use PanelBundle\Form\CustomerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;

class OrdersController extends Controller
{   
    /**
     * @Route("/panel/orders/list")
     */
    public function listAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }
        $date = array();
        $form = $this->createFormBuilder($date)
            ->setAction($this->generateUrl('panel_orders_list'))
            ->setMethod('POST')
            ->add('dateFrom', TextType::class, [
                'attr' => array('class' => 'form-control', 'id' => 'dateFrom'),
                'label' => 'Date From',
                'required' => false,
                'constraints' => array(
                    new Date())
            ])
            ->add('dateTo', TextType::class, [
                'attr' => array('class' => 'form-control'),
                'label' => 'Date To',
                'required' => false,
                'constraints' => array(
                    new Date())
            ])
            ->add('submit', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary btn-md saveButton right'),
                'label' => 'Search',
            ));
        
        if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $form->add('user', EmailType::class, [
                'attr' => array('class' => 'form-control'),
                'label' => "User\'s Email",
                'required' => false,
                'constraints' => array(
                    new Email())
            ]);
        }
        
        $form = $form->getForm();
        
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $orders = array();
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            
            $isAdmin = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') ? true : false;
            $dateFrom = $form->get('dateFrom')->getData();
            $dateTo = $form->get('dateTo')->getData();
            $userEmail = $isAdmin == true ? $form->get('user')->getData() : '';
            
            if($dateFrom == '' && $dateTo == '' && $userEmail == '') {
                if(!$isAdmin) {
                    $orders = $user->getOrders();
                } else {
                    $orders = $em->getRepository("OrderBundle\Entity\UserOrder")->findAll();
                }
            } else {
                $query = $em->getRepository('OrderBundle:UserOrder')
                        ->createQueryBuilder('o');
                
                if($userEmail != '') {
                    $query->leftJoin('o.user', 'u')
                          ->where('u.email = :email')
                          ->setParameter('email', $userEmail);
                }
                
                if($dateFrom != '') {
                    if($userEmail != '') {
                        $query->andWhere('o.dateAdded >= :dateFrom');
                    } else {
                        $query->where('o.dateAdded >= :dateFrom');
                    }
                    $query->setParameter('dateFrom', $dateFrom);
                }
                
                if($dateTo != '') {
                    if($userEmail != '' || $dateTo != '') {
                        $query->andWhere('o.dateAdded <= :dateTo');
                    } else {
                        $query->where('o.dateAdded <= :dateTo');
                    }
                    $query->setParameter('dateTo', $dateTo);
                }
                
                $orders = $query->orderBy('o.dateAdded', 'ASC')
                                ->getQuery()
                                ->getResult();
            }       
        } else {
            if(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                $orders = $user->getOrders();
            } else {
                $orders = $em->getRepository("OrderBundle\Entity\UserOrder")->findAll();
            }
        }
        
        $totalSum = 0;
        $totalProvision = 0;

        foreach($orders as $order) {
            $totalSum += $order->getSum();
            $totalProvision += $order->getProvision();
        }
        
        return $this->render('PanelBundle:Orders:list.html.twig', 
                array('orders' => $orders,
                      'form' => $form->createView(),
                      'totalSum' => $totalSum,
                      'totalProvision' => $totalProvision));
    }
    
}
