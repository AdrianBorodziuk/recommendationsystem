<?php

namespace PanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DiscountController extends Controller
{
    
    /**
     * @Route("/panel/discount")
     */
    public function indexAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }
        
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder($user)
            ->setAction($this->generateUrl('panel_discount_index'))
            ->setMethod('POST')
            ->add('discountCode', TextType::class, array(
                'attr' => array('class' => 'form-control'),
                'label' => 'Discount Code',
                'data' => $user->getDiscountCode(),
                'constraints' => array(
                    new NotBlank()),  
            ))
            ->add('submit', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary btn-md saveButton'),
                'label' => 'Save',
            ))
            ->getForm();
                
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'success',
                'Your discount code has been saved.'
            );
        }

        return $this->render('PanelBundle:Discount:index.html.twig', 
            array("form" => $form->createView()));
    }
}