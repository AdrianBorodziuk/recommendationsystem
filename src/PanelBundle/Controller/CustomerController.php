<?php

namespace PanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use UserBundle\Entity\User;
use PanelBundle\Form\CustomerType;

class CustomerController extends Controller
{
    /**
     * @Route("/panel/customer/add")
     */
    public function addAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }
        $user = new User();
        
        $form = $this->createForm(CustomerType::class, $user, array(
            'action' => $this->generateUrl('panel_customer_add'),
            'method' => 'POST',
        ));
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setRoles('ROLE_USER');
            $user->setRegisterDate();
            $user->setDiscountCode('');

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
            $this->addFlash(
                'success',
                'New account has been created.'
            );

            return $this->redirectToRoute('panel_customer_list');
        }
        return $this->render('PanelBundle:Customer:add.html.twig', 
                array("form" => $form->createView()));
    }
    
    /**
     * @Route("/panel/customer/list")
     */
    public function listAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }
        
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $users = $em->getRepository('UserBundle\Entity\User')->findAll();

        return $this->render('PanelBundle:Customer:list.html.twig', array('users' => $users));
    }
    
    /**
     * @Route("/panel/customer/edit/{idCustomer}")
     */
    public function editAction($idCustomer, Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }

        //$user = $this->get('security.token_storage')->getToken()->getUser();
        
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle\Entity\User')->find($idCustomer);
        if(is_null($user)) {
            $this->addFlash("error", "This Customer does not exist.");
            return $this->redirectToRoute('panel_customer_list');
        }
        
//        if($product->getUser()->getId() !== $user->getId()) {
//            $this->addFlash("error", "This product does not belong to you.");
//            return $this->redirectToRoute('panel_product_list');
//        }
        
        $form = $this->createForm(CustomerType::class, $user, array(
            'action' => $this->generateUrl('panel_customer_edit', array('idCustomer' => $idCustomer)),
            'method' => 'POST',
        ));
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            
            if($user->getPlainPassword() != '') {
                $password = $this->get('security.password_encoder')
                    ->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);
            }
            
            $em->persist($user);
            $em->flush();
            
            $this->addFlash(
                'success',
                'Your changes were saved.'
            );

            return $this->redirectToRoute('panel_customer_list');
        }
        
        
        return $this->render('PanelBundle:Customer:edit.html.twig', 
                array("form" => $form->createView(),
                      "user" => $user
                ));
    }
    
    /**
     * @Route("/panel/customer/remove/{idCustomer}")
     */
    public function removeAction($idCustomer, Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle\Entity\User')->find($idCustomer);
        if(is_null($user)) {
            $this->addFlash("error", "Customer does not exist.");
            return $this->redirectToRoute('panel_product_list');
        }
        
        $em->remove($user);
        
        $em->flush();
        
        $this->addFlash("success", "Customer's account has been removed.");
        return $this->redirectToRoute('panel_customer_list');
        
    }
    
    /**
     * @Route("/panel/customer/show/{idCustomer}")
     */
    public function showAction($idCustomer)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }
        
        $em = $this->getDoctrine()->getManager();
        $customer = $em->getRepository('UserBundle\Entity\User')->find($idCustomer);
        
        if(is_null($customer)) {
            $this->addFlash("error", "Customer does not exist.");
            return $this->redirectToRoute('panel_product_list');
        }

        return $this->render('PanelBundle:Customer:show.html.twig', 
                array('customer' => $customer));
    }
    
}
