<?php

namespace PanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use ProductBundle\Entity\Product;
use UserBundle\Entity\User;
use ProductBundle\Form\ProductType;
use ProductBundle\Form\ProductEditType;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormError;

class ProductController extends Controller
{
    /**
     * @Route("/panel/product/add")
     */
    public function addAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }
        
        $product = new Product();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        
        $form = $this->createForm(ProductType::class, $product, array(
            'action' => $this->generateUrl('panel_product_add'),
            'method' => 'POST',
        ));
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            
            if(!$product->upload()) {
                $error = new FormError("Selected file is not a valid image");
                $form->get('image')->addError($error);
                
                return $this->render('PanelBundle:Product:add.html.twig', 
                    array("form" => $form->createView(),
                ));
            }
            
            $product->setUser($user);
            $product->setLastModified();
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
            
            $this->addFlash(
                'success',
                'Your product is saved now.'
            );

            return $this->redirectToRoute('panel_product_list');
        }
        
        return $this->render('PanelBundle:Product:add.html.twig', 
                array("form" => $form->createView(),
                ));
    }
    
    /**
     * @Route("/panel/product/list")
     */
    public function listAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }
        
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $products = $user->getProducts();

        return $this->render('PanelBundle:Product:list.html.twig', array('products' => $products));
    }
    
    /**
     * @Route("/panel/product/edit/{idProduct}")
     */
    public function editAction($idProduct, Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ProductBundle\Entity\Product')->find($idProduct);
        if(is_null($product)) {
            $this->addFlash("error", "Product does not exist.");
            return $this->redirectToRoute('panel_product_list');
        }
        
        if($product->getUser()->getId() !== $user->getId()) {
            $this->addFlash("error", "This product does not belong to you.");
            return $this->redirectToRoute('panel_product_list');
        }
        
        $form = $this->createForm(ProductEditType::class, $product, array(
            'action' => $this->generateUrl('panel_product_edit', array('idProduct' => $idProduct)),
            'method' => 'POST',
        ));
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            
            if($form->get('image')->getData() != null && !$product->upload()) {
                $error = new FormError("Selected file is not a valid image");
                $form->get('image')->addError($error);
                
                $this->addFlash(
                    'error',
                    'Please correct form fields.'
                );
                
                return $this->render('PanelBundle:Product:edit.html.twig', 
                    array("form" => $form->createView(), 'product' => $product
                ));
            }
            
            $product->setLastModified();
            
            $em->persist($product);
            $em->flush();
            
            $this->addFlash(
                'success',
                'Your changes were saved.'
            );

            return $this->redirectToRoute('panel_product_list');
        }
        
        
        return $this->render('PanelBundle:Product:edit.html.twig', 
                array("form" => $form->createView(),
                      "product" => $product
                ));
    }
    
    /**
     * @Route("/panel/product/remove/{idProduct}")
     */
    public function removeAction($idProduct, Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ProductBundle\Entity\Product')->find($idProduct);
        if(is_null($product)) {
            $this->addFlash("error", "Product does not exist.");
            return $this->redirectToRoute('panel_product_list');
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        
        if($product->getUser()->getId() !== $user->getId()) {
            $this->addFlash("error", "This product does not belong to you.");
            return $this->redirectToRoute('panel_product_list');
        }
        
        $user->removeProduct($product);
        $em->persist($user);
        $em->remove($product);
        
        $em->flush();
        
        $this->addFlash("success", "Product has been removed.");
        return $this->redirectToRoute('panel_product_list');
        
    }
}
