<?php

namespace ProductBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Constraints\File;

class ProductEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($options['action'])
            ->setMethod($options['method'])
            ->add('price', TextType::class, array(
                'attr' => array('class' => 'form-control'),
                'label' => 'Price',
                'constraints' => array(
                    new GreaterThan(0),
                    new NotBlank()
            )))
            ->add('url', TextType::class, array(
                'attr' => array('class' => 'form-control'),
                'label' => 'Product URL',
                'constraints' => array(
                    new Url(),
                    new NotBlank()
            )))
            ->add('image', FileType::class, array(
                'attr' => array('class' => 'form-control'),
                'required' => false,
                'label' => 'Image',
                'constraints' => array(
                    new File()
            )))
            ->add('submit', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary btn-md saveButton'),
                'label' => 'Save',
            ));
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ProductBundle\Entity\Product',
        ));
    }
}

