<?php

namespace ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="ProductBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $name;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\User", inversedBy="products")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $price;
    
    /**
     * @ORM\Column(type="string", length=128)
     */
    private $url;
        
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $imageUrl;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $lastModified;
    
    /**
    * @var string $file                                      <-- @var UploadedFile $file
    * 
    * @Assert\Image()
    */
    private $image;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Product
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set imageUrl
     *
     * @param string $imageUrl
     *
     * @return Product
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
       
        return $this;
    }

    /**
     * Get imageUrl
     *
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }
    
    /**
     * Get image
     *
     * @return UploadedFile
     */
    public function getImage()
    {
        return $this->image;
    }
    

    
    /**
     * Set image
     * 
     * @param UploadedFile $image
     *
     * @return Product
     */
    public function setImage($image)
    {
        $this->image = $image;
        
        return $this;
    }
    
    public function upload() {
        
        if (!is_null($this->image) && is_array(getimagesize($this->image->getRealPath()))) {
            $extension = explode(".", $this->image->getClientOriginalName());
            $fileName = sha1($this->image->getClientOriginalName() . rand(1, 999999)) . '.' . end($extension);

            $this->image->move(
                $this->getUploadDir(),
                $fileName
            );

            $this->setImageUrl($this->getUploadDir() . '/' . $fileName);
            $this->image = null;  
            return true;
        }

        $this->image = null;        
        return false;
    }    
    /**
     * Set user
     *
     * @param \ProductBundle\Entity\User $user
     *
     * @return Product
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ProductBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    public function getLastModified() {
        return $this->lastModified;
    }
    
    public function setLastModified() {
        $this->lastModified = new \DateTime("now");
    }

    protected function getUploadDir()
    {
        return 'uploads/images';
    }
}
