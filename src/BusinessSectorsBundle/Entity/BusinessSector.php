<?php

namespace BusinessSectorsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BusinessSector
 *
 * @ORM\Table(name="business_sector")
 * @ORM\Entity(repositoryClass="BusinessSectorsBundle\Repository\BusinessSectorRepository")
 */
class BusinessSector
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $name;
    
    /**
     * @ORM\OneToMany(targetEntity="\BusinessSectorsBundle\Entity\UserBusinessSector", mappedBy="businessSector")
     */
    private $usersBusinessSectors;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * Set name
     *
     * @param string $name
     * 
     * @return BusinessSector
     */
    public function setName($name) {
        $this->name = $name;
        
        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usersBusinessSectors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add usersBusinessSector
     *
     * @param \BusinessSectorsBundle\Entity\UserBusinessSector $usersBusinessSector
     *
     * @return BusinessSector
     */
    public function addUsersBusinessSector(\BusinessSectorsBundle\Entity\UserBusinessSector $usersBusinessSector)
    {
        $this->usersBusinessSectors[] = $usersBusinessSector;

        return $this;
    }

    /**
     * Remove usersBusinessSector
     *
     * @param \BusinessSectorsBundle\Entity\UserBusinessSector $usersBusinessSector
     */
    public function removeUsersBusinessSector(\BusinessSectorsBundle\Entity\UserBusinessSector $usersBusinessSector)
    {
        $this->usersBusinessSectors->removeElement($usersBusinessSector);
    }

    /**
     * Get usersBusinessSectors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsersBusinessSectors()
    {
        return $this->usersBusinessSectors;
    }
}
