<?php

namespace BusinessSectorsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserBusinessSector
 *
 * @ORM\Table(name="user_business_sector")
 * @ORM\Entity(repositoryClass="BusinessSectorsBundle\Repository\UserBusinessSectorRepository")
 */
class UserBusinessSector
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\User", inversedBy="businessSectors")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="\BusinessSectorsBundle\Entity\BusinessSector", inversedBy="usersBusinessSectors")
     * @ORM\JoinColumn(name="business_sector_id", referencedColumnName="id")
     */
    private $businessSector;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return UserBusinessSector
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set businessSector
     *
     * @param \BusinessSectorsBundle\Entity\BusinessSector $businessSector
     *
     * @return UserBusinessSector
     */
    public function setBusinessSector(\BusinessSectorsBundle\Entity\BusinessSector $businessSector = null)
    {
        $this->businessSector = $businessSector;

        return $this;
    }

    /**
     * Get businessSector
     *
     * @return \BusinessSectorsBundle\Entity\BusinessSector
     */
    public function getBusinessSector()
    {
        return $this->businessSector;
    }
}
