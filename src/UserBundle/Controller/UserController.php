<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use UserBundle\Entity\User;
use UserBundle\Form\UserType;

class UserController extends Controller
{
    /**
     * @Route("/register")
     */
    public function registerAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }
        $user = new User();
        
        $form = $this->createForm(UserType::class, $user, array(
            'action' => $this->generateUrl('user_user_register'),
            'method' => 'POST',
        ));
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setRoles('ROLE_USER');
            $user->setDiscountCode('');
            $user->setRegisterDate();

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
            $this->addFlash(
                'success',
                'Your account were created. You can now log in.'
            );

            return $this->redirectToRoute('homepage');
        }
        return $this->render('UserBundle:User:register.html.twig', 
                array("form" => $form->createView()));
    }
    
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request) {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();
             
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'last_username' => $lastUsername,
            'error'         => $error,
            'show_popup'    => 1
        ]);
    }
    
    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(Request $request) {
        $this->addFlash(
                'notice',
                'You have been logged out'
        );
        return $this->redirectToRoute('homepage');
    }
    
}
