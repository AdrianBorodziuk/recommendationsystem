<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($options['action'])
            ->setMethod($options['method'])
            ->add('username', EmailType::class, array(
                'attr' => array('class' => 'form-control'),
                'label' => 'Email',
                'constraints' => array(
                    new Email(),
                    new NotBlank())
            ))
            ->add('name', TextType::class, array(
                'attr' => array('class' => 'form-control'),
                'label' => 'Firstname and Surename',
                'constraints' => array(
                    new NotBlank())
            ))
            ->add('storeUrl', TextType::class, array(
                'attr' => array('class' => 'form-control'),
                'label' => 'Store URL',
                'constraints' => array(
                    new Url(),
                    new NotBlank()
            )))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Password', 'attr' => array('class' => 'form-control')),
                'second_options' => array('label' => 'Repeat Password', 'attr' => array('class' => 'form-control')),
                'attr' => array('class' => 'form-control')
            ))
            ->add('termsAccepted', CheckboxType::class, array(
                'mapped' => false,
                'constraints' => new IsTrue(),
                'label' => 'I agreed with Terms of Service'
            ))
            ->add('submit', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary btn-md saveButton'),
                'label' => 'Register'
            ));
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User'
        ));
    }
}

