<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $email;
    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $name;
    
    /**
     * @ORM\OneToMany(targetEntity="\ProductBundle\Entity\Product", mappedBy="user")
     */
    protected $products;
    
    /**
     * @ORM\OneToMany(targetEntity="\OrderBundle\Entity\UserOrder", mappedBy="user")
     */
    protected $orders;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;
    
    
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;
    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $storeUrl;
    
    /**
     * @ORM\OneToMany(targetEntity="\BusinessSectorsBundle\Entity\UserBusinessSector", mappedBy="user")
     */
    private $businessSectors;
    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $discountCode;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $registerDate;
    
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $roles;
    
    private $tmpBusinessSectors;
    
    private $plainPassword;
    
    
    public function __construct()
    {
        $this->isActive = true;
        $this->products = new ArrayCollection();
        $this->businessSectors = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function setUsername($username)
    {
        $this->email = $username;
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getSalt()
    {
        return null;
    }
    
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }
    
    public function getDiscountCode()
    {
        return $this->discountCode;
    }

    public function setDiscountCode($discountCode)
    {
        $this->discountCode = $discountCode;
    }
    
    public function getStoreUrl()
    {
        return $this->storeUrl;
    }

    public function setStoreUrl($storeUrl)
    {
        $this->storeUrl = $storeUrl;
    }
    
    public function setRoles($roles) {
        $this->roles = $roles;
        
        return $this;
    }
    
    public function getRoles()
    {
        return array($this->roles);
    }
    
    public function getIsActive() {
        return $this->isActive;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            $this->isActive,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->isActive,
        ) = unserialize($serialized);
    }
    



    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Add product
     *
     * @param \UserBundle\Entity\Product $product
     *
     * @return User
     */
    public function addProduct(\ProductBundle\Entity\Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \UserBundle\Entity\Product $product
     */
    public function removeProduct(\ProductBundle\Entity\Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add businessSector
     *
     * @param \BusinessSectorsBundle\Entity\UserBusinessSector $businessSector
     *
     * @return User
     */
    public function addBusinessSector(\BusinessSectorsBundle\Entity\UserBusinessSector $businessSector)
    {
        $this->businessSectors[] = $businessSector;

        return $this;
    }

    /**
     * Remove businessSector
     *
     * @param \BusinessSectorsBundle\Entity\UserBusinessSector $businessSector
     */
    public function removeBusinessSector(\BusinessSectorsBundle\Entity\UserBusinessSector $businessSector)
    {
        $this->businessSectors->removeElement($businessSector);
    }

    /**
     * Get businessSectors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBusinessSectors()
    {
        return $this->businessSectors;
    }
    
    public function getTmpBusinessSectors()
    {
        return $this->tmpBusinessSectors;
    }
    
    public function setTmpBusinessSectors($tmpBusinessSectors)
    {
        $this->tmpBusinessSectors = $tmpBusinessSectors;
        
        return $this;
    }
    
    public function getRegisterDate() {
        return $this->registerDate;
    }
    
    public function setRegisterDate() {
        $this->registerDate = new \DateTime("now");
    }


    /**
     * Add order
     *
     * @param \OrderBundle\Entity\UserOrder $order
     *
     * @return User
     */
    public function addOrder(\OrderBundle\Entity\UserOrder $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \OrderBundle\Entity\UserOrder $order
     */
    public function removeOrder(\OrderBundle\Entity\UserOrder $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }
    
    public function getOrdersSum() {
        $sum = 0;
        foreach($this->getOrders() as $order) {
            $sum += $order->getSum();
        }
        return $sum;
    }
    
    public function getOrdersProvision() {
        $sum = 0;
        foreach($this->getOrders() as $order) {
            $sum += $order->getProvision();
        }
        return $sum;
    }
    
    public function getPlainBusinessSectors() {
        $businessSectors = '';
        
        $isFirstLoop = true;
        foreach($this->getBusinessSectors() as $sector) {
            if(!$isFirstLoop) {
                $businessSectors .= ', ';
            }
            $businessSectors .= $sector->getBusinessSector()->getName();
            $isFirstLoop = false;
        }
        
        return $businessSectors != '' ? $businessSectors : 'User does not have business sectors set';
    }
    
    public function getNumberOfProducts() {
        return count($this->getProducts());
    }
    
    public function getNumberOfOrders() {
        return count($this->getOrders());
    }
}
